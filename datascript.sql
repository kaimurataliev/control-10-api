CREATE DATABASE `control_10`;
USE `control_10`;

CREATE TABLE `news` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `title` VARCHAR (200) NOT NULL,
    `description` TEXT,
    `image` VARCHAR(250),
    `date` DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE `comments` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `news_id` INT NOT NULL,
    INDEX `FK_news_idx` (`news_id`),
    CONSTRAINT FK_news
		FOREIGN KEY (`news_id`)
        REFERENCES `news` (`id`),
	`author` VARCHAR(250),
    `comment` TEXT NOT NULL
);



