const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');

const config = require('../../config');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (rq, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const createRouter = (db) => {
    router.get('/news', (req, res) => {
        db.query("SELECT `id`, `title`, `image`, `date` FROM `news`", (error, results) => {
            if (error) res.status(400).send(error.sqlMessage);
            res.send(results)
        })
    });

    router.get('/comments', (req, res) => {
        if (req.query.news_id) {
            db.query('SELECT * FROM `comments` WHERE news_id = ' + req.query.news_id + ';', (error, result) => {
                if (error) {
                    console.log(error)
                } else {
                    res.send(result);
                }
            })
        } else {
            db.query("SELECT * FROM `comments`", (error, result) => {
                if (error) {
                    res.status(400).send({error, errorMessage: 'no comments yet'})
                } else {
                    res.send(result)
                }
            })
        }

    });

    router.post('/news', upload.single('image'), (req, res) => {
        const news = req.body;

        if (req.file) {
            news.image = req.file.filename;
        } else {
            news.image = null;
        }

        if (!req.body.description) {
            res.status(400).send({errorMessage: 'You have to enter news description'})
        }

        db.query(
            'INSERT INTO `news` (`title`, `description`, `image`)' +
            'VALUES (?, ?, ?)',
            [news.title, news.description, news.image],

            (error, results) => {
                if (error) console.log(error);

                res.send(results);
            }
        )
    });

    router.get('/:type/:id', (req, res) => {
        db.query('SELECT * FROM `' + req.params.type + '` WHERE id = ' + req.params.id + ';', (error, result) => {
            if (error) console.log(error);
            res.send(result[0]);
        })
    });

    router.delete('/:type/:id', (req, res) => {
        db.query('DELETE FROM `' + req.params.type + '` WHERE id = ' + req.params.id + ';', (error) => {
            if (error) {
                res.status(400).send(error.sqlMessage);
            } else {
                res.send({message: 'Item was deleted'})
            }
        })
    });

    router.post('/comments', (req, res) => {
        if (!req.body.author) {
            req.body.author = 'Anonymous';
        }

        const comment = req.body;

        db.query(
            'INSERT INTO `comments` (`news_id`, `author`, `comment`)' +
            'VALUES (?, ?, ?)',
            [comment.news_id, comment.author, comment.comment],

            (error, results) => {
                if (error) console.log(error);

                res.send(results);
            }
        )
    });
    return router
};

module.exports = createRouter;