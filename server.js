const express = require('express');
const news = require('./app/news/news');
const cors = require('cors');
const mysql = require('mysql');
const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'username',
    password: '1234',
    database: 'control_10'
});

connection.connect((error) => {

    if (error) console.log(error);

    app.use('/', news(connection));

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});

